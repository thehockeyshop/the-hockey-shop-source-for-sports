The Hockey Shop Source For Sports superstore in Surrey, BC Canada. Our 17,000 sq. ft. retail location is the largest independent hockey store on the West Coast of Canada. With over forty hockey specialists, including ten goal specialists and six professional skate sharpeners: We Know Hockey.

Address: 10280 City Parkway, Surrey, BC V3T 4C2, Canada

Phone: 604-589-8299
